package gcp;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.compute.Compute;
import com.google.api.services.compute.ComputeScopes;
import com.google.api.services.compute.model.AccessConfig;
import com.google.api.services.compute.model.AttachedDisk;
import com.google.api.services.compute.model.AttachedDiskInitializeParams;
import com.google.api.services.compute.model.Instance;
import com.google.api.services.compute.model.InstanceList;
import com.google.api.services.compute.model.NetworkInterface;
import com.google.api.services.compute.model.Operation;
import com.google.api.services.compute.model.ServiceAccount;

/**
 * Demo for the Java API of Google Cloud Compute Engine 
 */
public class ComputeEngineSample
{

//
//
//You'll need to change the values of the following
//
//
	/** Set the name of the sample VM instance to be created. */
	private static final String VM_NAME = "someName";
	
	/** Set PROJECT_ID to your Project ID from the Overview pane in the Developers console. */
	private static final String PROJECT_ID = "someProject";

	/** Set the path to the json filename containing your credentials to the Google Cloud API */
	private static final String JSON_GCP_CREDENTIALS_FILENAME = "somePath";

	
	
	
//These values should be fine without modification
	/** Set Compute Engine zone */
	private static final String ZONE_NAME = "us-central1-a";

	/** Set the time out limit for operation calls to the Compute Engine API. */
	private static final long OPERATION_TIMEOUT_MILLIS = 60 * 1000;

	/** Not sure what this is, but it cannot be empty */
	private static final String APPLICATION_NAME = "NotSureWhatThisIsAbout";

	
			
	

//You can leave these as they are, but you might want to change then eventuall	  
	/** Set the path of the OS image for the sample VM instance to be created.  */
	/** To see the list of available paths go to: https://console.cloud.google.com/compute/images?tab=images while logged in to your google cloud account */
	private static final String SOURCE_IMAGE_FULL_PATH = "https://www.googleapis.com/compute/beta/projects/ubuntu-os-cloud/global/images/ubuntu-2004-focal-v20210927";

	/** Set the path of the machine type you want to use to build the VM. Be aware of the cost. f1-micro is the cheapest one. */
	private static String MACHINE_TYPE_PATH = "https://www.googleapis.com/compute/v1/projects/" + PROJECT_ID + "/zones/" + ZONE_NAME + "/machineTypes/f1-micro";



	/** Global instance of the HTTP transport. */
	//private HttpTransport httpTransport;
	private GoogleCredential credential;
	private Compute compute;


	public ComputeEngineSample() throws GeneralSecurityException, IOException
	{
		HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();

		this.credential = GoogleCredential.fromStream(new FileInputStream(JSON_GCP_CREDENTIALS_FILENAME));

		if (credential.createScopedRequired()) {
			List<String> scopes = new ArrayList<>();
			// Set Google Cloud Storage scope to Full Control.
			scopes.add(ComputeScopes.DEVSTORAGE_FULL_CONTROL);
			// Set Google Compute Engine scope to Read-write.
			scopes.add(ComputeScopes.COMPUTE);
			credential = credential.createScoped(scopes);
		}

		
		JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
		this.compute = new Compute.Builder(httpTransport, JSON_FACTORY, credential)
				.setApplicationName(APPLICATION_NAME)
				.build();
	}

	private String getIPForVM(String vmName) throws IOException
	{
		System.out.println("================== Listing VM IP ==================");
		Compute.Instances.List instances = compute.instances().list(PROJECT_ID, ZONE_NAME);
		InstanceList list = instances.execute();
		
		if (list.getItems() == null)
		{
			System.out.println(
					"No instances found. Sign in to the Google Developers Console and create "
							+ "an instance at: https://console.developers.google.com/");
		}
		else
		{
			for (Instance instance : list.getItems())
			{
				
				if (instance.getName().equals(vmName))
				{
					ArrayList<AccessConfig> l = (ArrayList)instance.getNetworkInterfaces().get(0).get("accessConfigs");
					AccessConfig ac = (AccessConfig)l.get(0);
					String ip = ac.get("natIP").toString();
					return ip;	
				}
			}
		}
		return "Cannot find IP";
	}
	

	private Operation generateStartIntanceOperation(String instanceName) throws IOException
	{
		System.out.println("================== Creating New VM Operation ==================");

		// Create VM Instance object with the required properties.
		Instance instance = new Instance();
		instance.setName(instanceName);
		instance.setMachineType(MACHINE_TYPE_PATH);

		// Add Network Interface to be used by VM Instance.
		NetworkInterface ifc = new NetworkInterface();
		ifc.setNetwork("https://www.googleapis.com/compute/v1/projects/" + PROJECT_ID + "/global/networks/default");
		List<AccessConfig> configs = new ArrayList<>();
		AccessConfig config = new AccessConfig();
		config.setType("ONE_TO_ONE_NAT");
		config.setName("External NAT");
		configs.add(config);
		ifc.setAccessConfigs(configs);
		instance.setNetworkInterfaces(Collections.singletonList(ifc));

		// Add attached Persistent Disk to be used by VM Instance.
		AttachedDisk disk = new AttachedDisk();
		disk.setBoot(true);
		disk.setAutoDelete(true);
		disk.setType("PERSISTENT");
		AttachedDiskInitializeParams params = new AttachedDiskInitializeParams();
		// Assign the Persistent Disk the same name as the VM Instance.
		params.setDiskName(instanceName);
		// Specify the source operating system machine image to be used by the VM Instance.
		params.setSourceImage(SOURCE_IMAGE_FULL_PATH);
		// Specify the disk type as Standard Persistent Disk
		params.setDiskType("https://www.googleapis.com/compute/v1/projects/" + PROJECT_ID + "/zones/"
				+ ZONE_NAME + "/diskTypes/pd-standard");
		disk.setInitializeParams(params);
		instance.setDisks(Collections.singletonList(disk));

		// Initialize the service account to be used by the VM Instance and set the API access scopes.
		ServiceAccount account = new ServiceAccount();
		account.setEmail("default");
		List<String> scopes = new ArrayList<>();
		//scopes.add("https://www.googleapis.com/auth/devstorage.full_control");
		scopes.add("https://www.googleapis.com/auth/compute");
		account.setScopes(scopes);
		instance.setServiceAccounts(Collections.singletonList(account));

		// Optional - Add a startup script to be used by the VM Instance.
		/*Metadata meta = new Metadata();
		Metadata.Items item = new Metadata.Items();
		item.setKey("startup-script-url");
		// If you put a script called "vm-startup.sh" in this Google Cloud Storage bucket, it will execute on VM startup.
		// This assumes you've created a bucket named the same as your PROJECT_ID
		// For info on creating buckets see: https://cloud.google.com/storage/docs/cloud-console#_creatingbuckets
		item.setValue("gs://" + PROJECT_ID + "/vm-startup.sh");
		meta.setItems(Collections.singletonList(item));
		instance.setMetadata(meta);*/
		System.out.println(instance.toPrettyString());
		Compute.Instances.Insert insert = compute.instances().insert(PROJECT_ID, ZONE_NAME, instance);
		return insert.execute();
	}


	private Operation.Error executeOperation(Operation operation, long timeout) throws Exception
	{
		long start = System.currentTimeMillis();
		final long POLL_INTERVAL = 5 * 1000;
		String zone = operation.getZone();  // null for global/regional operations
		if (zone != null)
		{
			String[] bits = zone.split("/");
			zone = bits[bits.length-1];
		}
		
		String status = operation.getStatus();
		String opId = operation.getName();
		
		while (operation != null && !status.equals("DONE"))
		{
			Thread.sleep(POLL_INTERVAL);
			long elapsed = System.currentTimeMillis() - start;
			if (elapsed >= timeout)
			{
				throw new InterruptedException("Timed out waiting for operation to complete");
			}
			System.out.println("waiting...");
			if (zone != null)
			{
				Compute.ZoneOperations.Get get = compute.zoneOperations().get(PROJECT_ID, zone, opId);
				operation = get.execute();
			}
			else
			{
				Compute.GlobalOperations.Get get = compute.globalOperations().get(PROJECT_ID, opId);
				operation = get.execute();
			}
			if (operation != null)
			{
				status = operation.getStatus();
			}
		}
		return operation == null ? null : operation.getError();
	}

	
	
	public static void main(String[] args) throws Exception
	{
		System.out.println("START");
		ComputeEngineSample computeEngine = new ComputeEngineSample();
		
		Operation op =computeEngine.generateStartIntanceOperation(VM_NAME);
		
		Operation.Error error = computeEngine.executeOperation(op, OPERATION_TIMEOUT_MILLIS);
		if (error == null)
		{
			System.out.println("Success!");
		}
		else
		{
			System.out.println(error.toPrettyString());
		}
		
		String ip = computeEngine.getIPForVM(VM_NAME);
		System.out.println(VM_NAME + ": " + ip);
		System.out.println("DONE");
	}
}
